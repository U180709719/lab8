package shapes3d;

import shapes2d.Circle;

public class Cylinder extends Circle {


    private double height;

    public Cylinder(double r, double h) {
        super(r);
        height = h;
    }

    public double Volume()
    {
        return this.Area()*height;
    }

    @Override
    public String toString() {
        return String.format("The Area of Cylinder is: "+this.Area()+"\n The volume of Cylinder is: "+this.Volume());
    }
}