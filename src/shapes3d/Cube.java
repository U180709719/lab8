package shapes3d;

import shapes2d.Square;

public class Cube extends Square{

    public Cube(double s) {
        super(s);
    }


    public double Volume()
    {
        return this.Area()*this.side;
    }

    @Override
    public String toString() {
        return String.format("The Area of Cube is: "+this.Area()+"\nThe volume of Cube is: "+this.Volume());
    }
}