package shapes2d;

public class Circle {
    public double radius;

    public Circle(double r)
    {
        radius = r;
    }

    public double Area()
    {
        return (radius*radius)*Math.PI;
    }

    @Override
    public String toString() {
        return String.format("The Area of Circle is: "+this.Area());
    }
}
