package shapes2d;

public class Square {
    public double side;

    public Square(double s)
    {
        side = s;
    }

    public double Area()
    {
        return side*side;
    }

    @Override
    public String toString() {
        return String.format("The Square of Area is: "+this.Area());
    }
}
