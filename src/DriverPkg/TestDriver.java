package DriverPkg;

import shapes2d.Circle;
import shapes2d.Square;
import shapes3d.Cube;
import shapes3d.Cylinder;

public class TestDriver {

    public static void main(String[] args) {

        Circle cricle1 = new Circle(2);
        System.out.println(cricle1.toString());

        Square sqaure1 =new Square(5);
        System.out.println(sqaure1.toString());

        Cylinder cylinder1 = new Cylinder(3, 5);
        System.out.println(cylinder1.toString());

        Cube cube1 = new Cube(6);
        System.out.println(cube1.toString());
    }

}